import express, { NextFunction, Request, Response } from 'express';
import bodyParser from 'body-parser';
import config from './config';
import mongoose from 'mongoose';
import cookieParser from 'cookie-parser';
import cookieSession from 'cookie-session';
import loginController from './auth.controller';
import passport from 'passport';
import { Strategy as PassportLocal } from 'passport-local';
import User, { User as UserModel } from './user.model';
import auth from './auth.service';
import { createServer } from 'http';
import { Server } from 'socket.io';

type SessionUser = {
    id: string,
    username: string
};

const app = express();
const port = 8080;
const staticFilesDir = 'client';
const session = cookieSession({
    keys: ['secret'],
    maxAge: 2 * 60 * 60 * 1000
});
const parser = cookieParser();

/** Connection to MongoDb */
mongoose
    .connect(config.mongo.url)
    .then(() => {
        console.log('Successfully connected to Mongo');
    })
    .catch((error) => {
        console.error(error);
    });

/** Parse the body of the request */
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(parser);
app.use(session);

/** Passport */
passport.use(new PassportLocal((username, password, done) => {
    User.findOne({username})
        .then(user => {
            if (!user) {
                console.log('no user exists with username', username);
                return done(null, false);
            }
            const isPasswordValid = auth.isValidPassword(password, user.passwordHash);
            if (isPasswordValid) {
                console.log('successfully authenticated', username);
                return done(null, user);
            }
            console.log('invalid password', username);
            return done(null, false);
        })
        .catch(error => done(error));
}));
passport.serializeUser((user, done) => {
    console.log('serialize user: ', user);
    const {id, username} = user as UserModel;
    done(null, {id, username});
});
passport.deserializeUser((user, done) => {
    console.log('deserialize user: ', user);
    const sessionUser = user as SessionUser;
    User.findById(sessionUser.id)
        .then(user => done(null, user))
        .catch(error => done(error));
});
app.use(passport.initialize());
app.use(passport.session());

app.use((req: Request, res: Response, next: NextFunction) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');

    if (req.method == 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }

    next();
});

app.use(express.static(staticFilesDir));

/** Routes */
app.get('/login', (req: Request, res: Response) => {
    res.sendFile('login.html', {root: staticFilesDir});
});
app.post('/login', loginController.authenticate);

app.get('/register', (req: Request, res: Response) => {
    res.sendFile('register.html', {root: staticFilesDir});
});
app.post('/register', loginController.addUser);

app.get('/logout', loginController.logout);

app.get('/', loginController.checkAuthMiddleware, (req: Request, res: Response) => {
    console.log('user in chat: ', req.user);
    res.sendFile('chat.html', {root: staticFilesDir});
});

/** Error handling */
app.use((req: Request, res: Response) => {
    const error = new Error('Not found');

    res.status(404).json({
        message: error.message
    });
});

const server = createServer(app);
const io = new Server(server, {});
let users: string[] = [];

io.use((socket, next) => {
    const req = socket.handshake as any;
    const res = {} as Response;
    parser(req, res, err => {
        if (err) {
            return next(err);
        }
        session(req, res, next as any);
    });
});

io.on('connection', socket => {
    const username = (socket.handshake as any).session.passport.user.username as string;
    if (!users.includes(username)) {
        users.push(username);
    }
    socket.on('joinclient', data => {
        console.log(data);
        console.log('socket-clients:', Object.keys(io.sockets.sockets));
        socket.emit('joinserver', {msg: `Привіт, ${username}!`, users, username: 'admin', currentUser: username});
        socket.broadcast.emit('joinserver', {msg: `До чату долучився ${username}`, users, username: 'admin'});
    });
    socket.on('newmessage', data => {
        console.log('new message received', data);
        console.log('socket-clients:', Object.keys(io.sockets.sockets));
        io.sockets.emit('newmessage', data);
    });
    socket.on('disconnect', reason => {
        console.log('disconnect request received', reason);
        if (reason === 'client namespace disconnect') {
            const username = (socket.handshake as any).session.passport.user.username as string;
            console.log('disconnecting', username);
            if (!users.includes(username)) {
                return;
            }
            users = users.filter(val => val !== username);
            socket.broadcast.emit('newmessage', {msg: `${username} покинув чат`, users, username: 'admin'});
        }
    });
});

server.listen(port, () => {
    console.log(`Server is listening on ${port}`);
    console.log('Users', users);
});

console.log(`Starting server on port ${port}`);
