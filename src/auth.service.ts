import crypto from 'crypto';

function isValidPassword(password: string, hash: string): boolean {
    const passwordHash = crypto.scryptSync(password, '', 256).toString('hex');
    return hash === passwordHash;
}

function genPasswordHash(password: string): string {
    return crypto.scryptSync(password, '', 256).toString('hex');
}

export default {isValidPassword, genPasswordHash};
