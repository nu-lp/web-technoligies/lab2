import mongoose, { Document, Schema } from 'mongoose';

export interface User extends Document {
    username: string;
    passwordHash: string;
}

const UserSchema: Schema = new Schema(
    {
        username: {
            type: String,
            unique: true,
            required: true
        },
        passwordHash: {
            type: String,
            required: true
        }
    },
    {
        versionKey: false
    }
);

export default mongoose.model<User>('User', UserSchema);
