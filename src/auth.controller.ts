import { NextFunction, Request, Response } from 'express';
import passport from 'passport';
import User from './user.model';
import auth from './auth.service';

const authenticate = passport.authenticate(
    'local',
    {
        successRedirect: '/',
        failureRedirect: '/login'
    });

const checkAuthMiddleware = (req: Request, res: Response, next: NextFunction) => {
    if (req.isAuthenticated()) {
        next();
    } else {
        res.redirect('/login');
    }
};

const addUser = (req: Request, res: Response) => {
    const newUser = new User({
        username: req.body.username,
        passwordHash: auth.genPasswordHash(req.body.password)
    });

    return newUser.save()
        .then((saved) => {
            console.log('registered new user', saved.username);
            return authenticate(req, res);
        })
        .catch((error) => {
            return res.status(500).json({
                message: error.message,
                error: error
            });
        });
};

const logout = (req: Request, res: Response) => {
    console.log('logout request');
    if (req.isAuthenticated()) {
        req.session = null;
    }
    return res.redirect('/login');
};


export default {authenticate, checkAuthMiddleware, addUser, logout};
