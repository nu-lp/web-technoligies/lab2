var socket;
var currentUser = '';

function addNewMessage(body) {
    const author = body.username;
    const messageContent = `
        <div class="msg__username">${body.username}</div>
        <div class="msg_text">${body.msg}</div>`;
    $('<div>').addClass('msg')
        .addClass(author === currentUser ? 'msg--my' : 'msg--other')
        .html(messageContent)
        .appendTo($('#messages'));
    document.getElementById('messages').scrollTop = document.getElementById('messages').scrollHeight;
}

function sendMessage() {
    const message = $('#message-text').val();
    const request = {msg: message, username: currentUser};
    console.log('sending new message', request);
    socket.emit('newmessage', request);
}

$(document).ready(function () {
    socket = io.connect('http://localhost:8080');
    socket.emit('joinclient', 'new client is connected');
    socket.on('joinserver', function (response) {
        if (response.currentUser) {
            currentUser = response.currentUser;
        }
        console.log(response);
        addNewMessage(response);
    });
    socket.on('newmessage', function (response) {
        console.log('received new message', response);
        addNewMessage(response);
    })

    $('#new-msg-form').submit(function (event) {
        sendMessage();
        $('#message-text').val('');
        event.preventDefault();
    });

    $('#logout-btn').click(function () {
        console.log('logout');
        socket.disconnect();
        currentUser = '';
        socket = undefined;
        $.get('http://localhost:8080/logout', function (data) {
            const loginPage = '/login';
            $(location).attr('href', loginPage);
        })
    });
});
